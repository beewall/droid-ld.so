package ld.a;

import android.app.*;
import android.os.*;
import android.widget.*;
import java.util.*;
import android.content.res.*;
import java.io.*;
import android.util.*;
import android.content.*;
import android.view.*;
import android.text.Html;
import android.text.method.LinkMovementMethod;

public class MainActivity extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		TextView textView = (TextView) findViewById(R.id.textview);
		String arch = System.getProperty("os.arch");
		String fullArch = android.os.Build.CPU_ABI;
		textView.setText("Please wait...");
		AssetManager assetMgr = getAssets();
		String filesDir = getFilesDir().getAbsolutePath();
		String appPackName = "ld";
		String ldArch = null;
		boolean supported = true;
		String DeadlyStreamUrl = "http://deadlystream.com/forum/topic/5826-tool-xoreos-tools-for-termux-on-android/";
		//String XDAForumsUrl = "https://forums.xda-developers.com/";
		String AppSourceURL = "https://gitlab.com/PorygonZRocks/droid-ld.so.git";
		if (arch.contains("arch64"))
		{
			ldArch = "aarch64";
			supported = true;
		}
		else if (arch.contains("armv7"))
		{
			ldArch = "armhf";
			supported = true;
		}
		else if (arch.contains("arm"))
		{
			ldArch = "armel";
			supported = true;
		}
		else if (arch.contains("x86_64"))
		{
			ldArch = "x86_64";
			supported = false;
			textView.setText(Html.fromHtml("<p>I haven't found a binary for " + ldArch + " yet 🙍. If you have a  " + ldArch + " Linux computer, you can give me the /lib/ld-linux.so.* file on <a href='" + DeadlyStreamUrl + "'>DeadlyStream</a>" + /*or <a href='" + XDAForumsUrl + "'>XDA</a>*/".</p>"));
			textView.setMovementMethod(LinkMovementMethod.getInstance());
		}
		else if (arch.contains("86"))
		{
			ldArch = "x86";
			supported = false;
			textView.setText(Html.fromHtml("<p>I haven't found a binary for " + ldArch + " yet 🙍. If you have a  " + ldArch + " Linux computer, you can give me the /lib/ld-linux.so.* file on <a href='" + DeadlyStreamUrl + "'>DeadlyStream</a>" + /*or <a href='" + XDAForumsUrl + "'>XDA</a>*/ ".</p>"));
			textView.setMovementMethod(LinkMovementMethod.getInstance());
		}
		else if (arch.contains("mips64"))
		{
			ldArch = "mips64";
			supported = true;
		}
		else if (arch.contains("mips"))
		{
			ldArch = "mips";
			supported = true;
		}
		else
		{
			ldArch = fullArch;
			supported = false;
			textView.setText(Html.fromHtml("<p>" + arch + "/" + fullArch + " is unsupported 🙍. Let me know about this on " + "<a href='" + DeadlyStreamUrl + "'>DeadlyStream</a>" + /*or <a href='" + XDAForumsUrl + "'>XDA</a>*/ "!<br />Make sure to include your architecture (" + arch + "/" + fullArch + "), and I'll see what I can do.</p>"));
			textView.setMovementMethod(LinkMovementMethod.getInstance());
		}
		if (supported) 
		{
			try
			{
				String ldRealPath = filesDir + "/" + ldArch;
				String ldPath = filesDir + "/ld";
				textView.setText("Extracting binary (" + ldArch + ")...");
				InputStream ldIS = assetMgr.open(ldArch);
				FileOutputStream ldOS = openFileOutput(ldArch, Context.MODE_PRIVATE);
				byte[] ldBytes = MiscUtils.inputStreamToByteArray(ldIS);
				ldOS.write(ldBytes);
				ldIS.close();
				ldOS.close();
				textView.setText(textView.getText() + " ✓<br />Setting permissions...");
				MiscUtils.exec("chmod 755 " + ldRealPath);
				textView.setText(textView.getText() + " ✓<br />Creating symlink...");
				MiscUtils.exec("ln -s " + ldRealPath + " " + ldPath);
				textView.setText(textView.getText() + " ✓<br />Done! Now install xoreos-tools in Termux. (See " + "<a href='" + DeadlyStreamUrl + "'>DeadlyStream</a>" + /* or <a href='" + XDAForumsUrl + "'>XDA</a>*/ ".[Note: I've only compiled the tools for armhf so far.])");
				textView.setText(textView.getText() + "<br /> <br /> <br />The included linkers are part of glibc (<a href='http://sourceware.org/git/?p=glibc.git'>source code</a>, which is licensed under the <a href='https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt'>GNU LGPLv2+</a> by the Free Software Foundation (though see <a href='https://sourceware.org/git/?p=glibc.git;a=blob_plain;f=LICENSES;hb=HEAD'>here</a> for full licensing details).");
				textView.setText(Html.fromHtml(textView.getText() + "<br />The source for this app is under the <a href='https://www.gnu.org/licenses/gpl.txt'>GPLv3+</a>, and hosted on " + "<a href='" + AppSourceURL + "'>GitLab</a>."));
				textView.setMovementMethod(LinkMovementMethod.getInstance());
			}
			catch (Exception e)
			{
				Log.e(appPackName, e.getMessage(), e);
			}
		}
	}
}
